creature_snakeman

[OBJECT:CREATURE]

[CREATURE:SNAKEMAN]
	[NAME:snakeman:snakemen:serpentfolk]
	[LARGE_ROAMING][FREQUENCY:5]
	[AMPHIBIOUS][UNDERSWIM]
	[BIOME:LAKE_TEMPERATE_FRESHWATER]
	[BIOME:LAKE_TROPICAL_FRESHWATER]
	[POPULATION_NUMBER:5:10]
	[CLUSTER_NUMBER:2:5]
	[CREATURE_TILE:'S'][COLOR:7:0:1]
	[INTELLIGENT]
	[SMELL_TRIGGER:35] 1 is min, 50 is default.
	
	--sounds.. loudness/distance (squares), commonness (1000 for alarm, 10k peaceful).
	[SOUND:ALERT:23:1200:VOCALIZATION:hiss:hisses:a loud hissing]
	[SOUND:PEACEFUL_INTERMITTENT:35:80000:VOCALIZATION:click:clicks:a clicking] --very rare but long-distance
	--SOUND:PEACEFUL_INTERMITTENT:20:27000:VOCALIZATION:sneeze:sneezes:a sneeze]
	[SOUND:PEACEFUL_INTERMITTENT:7:25000:VOCALIZATION:burp:burps:an odd burp]
	[SOUND:PEACEFUL_INTERMITTENT:7:18000:VOCALIZATION:spit:spits:somebody spitting]
	[SOUND:PEACEFUL_INTERMITTENT:15:19500:VOCALIZATION:talk:talks:sibilant conversation]
	--SOUND:PEACEFUL_INTERMITTENT:13:24000:VOCALIZATION:pray:prays:a prayer]
	[SOUND:PEACEFUL_INTERMITTENT:4:16000:VOCALIZATION:sigh:sighs:a sigh] --shortest distance/loudness
	[SOUND:PEACEFUL_INTERMITTENT:16:17000:VOCALIZATION:swear:swears:somebody sssswearing]
	[SOUND:PEACEFUL_INTERMITTENT:11:19000:VOCALIZATION:mutter:mutters:somebody muttering]
	[SOUND:PEACEFUL_INTERMITTENT:8:11000:VOCALIZATION:grunt:grunts:a grunt]
	[SOUND:PEACEFUL_INTERMITTENT:6:85000:VOCALIZATION:cough:coughs:a quiet cough]
	
	[PROFESSION_NAME:LASHER:halberdier:halberdiers]
	[PROFESSION_NAME:MASTER_LASHER:elite halberdier:elite halberdiers]

	[VIEWRANGE:23]
	[NATURAL_SKILL:DISCIPLINE:1] to fix cowardice
	[LISP]
	[CHILD:1]
	[EQUIPS]
	[CANOPENDOORS]
	[ALL_ACTIVE]
	[LARGE_PREDATOR][EVIL]
	[CASTE:FEMALE_SALAMANDER]
		[POP_RATIO:20]
		[FEMALE]
		[LITTERSIZE:3:7]
		[DESCRIPTION:A scary scaly creature with a humanoid torso, with a bosom. She emits an aura of heat and her eyes burn bright.]
		[CASTE_NAME:salamander:salamanders:salamander]
		[GENERAL_CHILD_NAME:salamander hatchling:salamander hatchlings]
		[PREFSTRING:fiery nature]
		[CAN_DO_INTERACTION:MATERIAL_EMISSION]
			[CDI:ADV_NAME:Hurl fireball]
			[CDI:USAGE_HINT:ATTACK]
			[CDI:FLOW:FIREBALL]
			[CDI:TARGET:C:LINE_OF_SIGHT]
			[CDI:TARGET_RANGE:C:15]
			[CDI:MAX_TARGET_NUMBER:C:2]
			[CDI:WAIT_PERIOD:80]
		[CAN_DO_INTERACTION:MATERIAL_EMISSION]
			[CDI:ADV_NAME:Spray jet of fire]
			[CDI:USAGE_HINT:ATTACK]
			[CDI:FLOW:FIREJET]
			[CDI:TARGET:C:LINE_OF_SIGHT]
			[CDI:TARGET_RANGE:C:5]
			[CDI:MAX_TARGET_NUMBER:C:1]
			[CDI:WAIT_PERIOD:80]
		[FIREIMMUNE][FIREIMMUNE_SUPER]
		--no legs!
		[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:703:505:274:1900:2900] 32 kph  [SWIMS_INNATE]	
		[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:1945:1504:1062:548:3100:4500] 16 kph, slower than goat
		[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph  [STANCE_CLIMBER][NATURAL_SKILL:CLIMBING:15]

		[BODY:HUMANOID_LEGLESS_NECK:TAIL_STANCE:HUMANOID_JOINTS:5FINGERS:THROAT:NECK:SPINE:2EYES:2LUNGS:HEART:GUTS:BRAIN:SKULL:MOUTH:TEETH:RIBCAGE]
		[BODY_DETAIL_PLAN:STANDARD_MATERIALS] --cow-tough
			[REMOVE_MATERIAL:SKIN]
			[REMOVE_MATERIAL:LEATHER]
			[REMOVE_MATERIAL:PARCHMENT]
			[REMOVE_MATERIAL:HAIR]
			[USE_MATERIAL_TEMPLATE:SCALE:THINSCALE_NOLEATHER_TEMPLATE]
		[BODY_DETAIL_PLAN:STANDARD_TISSUES]
			[REMOVE_TISSUE:SKIN]
			[REMOVE_TISSUE:HAIR]
			[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
		[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
		[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
		 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
			[TL_MAJOR_ARTERIES]
		[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
		[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
		[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
		[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
		[HAS_NERVES]
		[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
		[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
		[CREATURE_CLASS:GENERAL_POISON]
		[GETS_WOUND_INFECTIONS]
		[GETS_INFECTIONS_FROM_ROT]
		[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
		[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
		[BODY_SIZE:0:0:2000]
		[BODY_SIZE:1:168:20000]
		[BODY_SIZE:2:0:70000]
		[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
		[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
		[MAXAGE:80:150]
		[ATTACK:PUNCH:BODYPART:BY_TYPE:GRASP]
			[ATTACK_SKILL:GRASP_STRIKE]
			[ATTACK_VERB:punch:punches]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_FLAG_WITH]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
		[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
			[ATTACK_SKILL:BITE]
			[ATTACK_VERB:bite:bites]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:SECOND]
			[ATTACK_FLAG_CANLATCH] okay
			[SPECIALATTACK_SUCK_BLOOD:25:50]
		[HOMEOTHERM:10067]
		[SWIMS_INNATE]
		[SELECT_MATERIAL:ALL]
			[MULTIPLY_VALUE:8]
			[HEATDAM_POINT:NONE]
			[IGNITE_POINT:NONE]
			[IF_EXISTS_SET_MELTING_POINT:55000]
			[IF_EXISTS_SET_BOILING_POINT:57000]
			[SPEC_HEAT:30000]
		[SELECT_MATERIAL:BLOOD]
		[PLUS_MATERIAL:PUS]
			[MELTING_POINT:10000]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:RED:1:CRIMSON:1:CARMINE:1:ORANGE:1]
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]
	[CASTE:MALE_SALAMANDER]
		[POP_RATIO:30]
		[MALE]
		[DESCRIPTION:A scaled creature with a humanoid torso. It emits an aura of heat and its eyes burn bright.]
		[CASTE_NAME:salamander:salamanders:salamander]
		[GENERAL_CHILD_NAME:salamander hatchling:salamander hatchlings]
		[PREFSTRING:fiery nature]
		[CAN_DO_INTERACTION:MATERIAL_EMISSION]
			[CDI:ADV_NAME:Hurl fireball]
			[CDI:USAGE_HINT:ATTACK]
			[CDI:FLOW:FIREBALL]
			[CDI:TARGET:C:LINE_OF_SIGHT]
			[CDI:TARGET_RANGE:C:15]
			[CDI:MAX_TARGET_NUMBER:C:2]
			[CDI:WAIT_PERIOD:30]
		[CAN_DO_INTERACTION:MATERIAL_EMISSION]
			[CDI:ADV_NAME:Spray jet of fire]
			[CDI:USAGE_HINT:ATTACK]
			[CDI:FLOW:FIREJET]
			[CDI:TARGET:C:LINE_OF_SIGHT]
			[CDI:TARGET_RANGE:C:5]
			[CDI:MAX_TARGET_NUMBER:C:1]
			[CDI:WAIT_PERIOD:30]
		
		[FIREIMMUNE][FIREIMMUNE_SUPER]
			--no legs!
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:703:505:274:1900:2900] 32 kph  [SWIMS_INNATE]	
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:1945:1504:1062:548:3100:4500] 16 kph, slower than goat
	[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph  [STANCE_CLIMBER][NATURAL_SKILL:CLIMBING:15]

		[BODY:HUMANOID_LEGLESS_NECK:TAIL_STANCE:HUMANOID_JOINTS:5FINGERS:THROAT:NECK:SPINE:2EYES:2LUNGS:HEART:GUTS:BRAIN:SKULL:MOUTH:TEETH:RIBCAGE]
		[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
			[REMOVE_MATERIAL:SKIN]
			[REMOVE_MATERIAL:LEATHER]
			[REMOVE_MATERIAL:PARCHMENT]
			[REMOVE_MATERIAL:HAIR]
			[USE_MATERIAL_TEMPLATE:SCALE:THINSCALE_NOLEATHER_TEMPLATE]
		[BODY_DETAIL_PLAN:STANDARD_TISSUES]
			[REMOVE_TISSUE:SKIN]
			[REMOVE_TISSUE:HAIR]
			[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
		[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
		[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
		 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
			[TL_MAJOR_ARTERIES]
		[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
		[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
		[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
		[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
		[HAS_NERVES]
		[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
		[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
		[CREATURE_CLASS:GENERAL_POISON]
		[GETS_WOUND_INFECTIONS]
		[GETS_INFECTIONS_FROM_ROT]
		[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
		[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
		[BODY_SIZE:0:0:2000]
		[BODY_SIZE:1:168:20000]
		[BODY_SIZE:2:0:70000]
		[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
		[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
		[MAXAGE:180:200]
		[ATTACK:PUNCH:BODYPART:BY_TYPE:GRASP]
			[ATTACK_SKILL:GRASP_STRIKE]
			[ATTACK_VERB:punch:punches]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_FLAG_WITH]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
		[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
			[ATTACK_SKILL:BITE]
			[ATTACK_VERB:bite:bites]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:SECOND]
			[ATTACK_FLAG_CANLATCH] okay
			[SPECIALATTACK_SUCK_BLOOD:25:50]
		[HOMEOTHERM:10067]
		[SWIMS_INNATE]
		[SELECT_MATERIAL:ALL]
			[MULTIPLY_VALUE:8]
			[HEATDAM_POINT:NONE]
			[IGNITE_POINT:NONE]
			[IF_EXISTS_SET_MELTING_POINT:55000]
			[IF_EXISTS_SET_BOILING_POINT:57000]
			[SPEC_HEAT:30000]
		[SELECT_MATERIAL:BLOOD]
		[PLUS_MATERIAL:PUS]
			[MELTING_POINT:10000]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:RED:1:CRIMSON:1:CARMINE:1:ORANGE:1]
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]
	[CASTE:FEMALE_STONEGAZER]
		[POP_RATIO:5]
		[FEMALE]
		[DESCRIPTION:A scaled creature with a humanoid torso and a head of a beautiful woman. Her head has snakes instead of hair. Beware her petrifying breath!]
		[CASTE_NAME:medusa:medusae:medusa]
		[GENERAL_CHILD_NAME:medusa hatchling:medusae hatchlings]
		[PREFSTRING:petrifying breath]
			--no legs!
	[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:703:505:274:1900:2900] 32 kph  [SWIMS_INNATE]	
	[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:1945:1504:1062:548:3100:4500] 16 kph, slower than goat
	[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph  [STANCE_CLIMBER][NATURAL_SKILL:CLIMBING:5]

		[BODY:HUMANOID_LEGLESS_NECK:TAIL_STANCE:HUMANOID_JOINTS:5FINGERS:THROAT:NECK:SPINE:2EYES:2LUNGS:HEART:GUTS:BRAIN:SKULL:MOUTH:TEETH:RIBCAGE:TENTACLE_HAIR]
		[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
			[REMOVE_MATERIAL:SKIN]
			[REMOVE_MATERIAL:LEATHER]
			[REMOVE_MATERIAL:PARCHMENT]
			[REMOVE_MATERIAL:HAIR]
			[USE_MATERIAL_TEMPLATE:SCALE:THINSCALE_NOLEATHER_TEMPLATE]
		[BODY_DETAIL_PLAN:STANDARD_TISSUES]
			[REMOVE_TISSUE:SKIN]
			[REMOVE_TISSUE:HAIR]
			[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
		[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
		[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
		 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
			[TL_MAJOR_ARTERIES]
		[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
		[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
		[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
		[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
		[HAS_NERVES]
		[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
		[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
		[CREATURE_CLASS:GENERAL_POISON]
		[GETS_WOUND_INFECTIONS]
		[GETS_INFECTIONS_FROM_ROT]
		[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
		[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
		[BODY_SIZE:0:0:2000]
		[BODY_SIZE:1:168:20000]
		[BODY_SIZE:2:0:70000]
		[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
		[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
		[MAXAGE:180:200]

		[USE_MATERIAL_TEMPLATE:MEDUSA_GAZE:CREATURE_EXTRACT_TEMPLATE]
			[STATE_NAME:ALL_SOLID:frozen petrifying liquid]
			[STATE_ADJ:ALL_SOLID:frozen petrifying liquid]
			[STATE_NAME:LIQUID:petrifying liquid]
			[STATE_ADJ:LIQUID:petrifying liquid]
			[STATE_NAME:GAS:petrifying gas]
			[STATE_ADJ:GAS:petrifying gas]
			[PREFIX:NONE]
			[ENTERS_BLOOD]
			[SYNDROME]
				[SYN_NAME:petrification]
				[SYN_AFFECTED_CLASS:GENERAL_POISON]
				[SYN_IMMUNE_CREATURE:SNAKEMAN:ALL]
				[SYN_INHALED]
				[CE_PARALYSIS:SEV:800:PROB:100:RESISTABLE:SIZE_DILUTES:START:5:PEAK:10:END:200]
		
		[CAN_DO_INTERACTION:MATERIAL_EMISSION]
 		[CDI:ADV_NAME:Gaze]
 		[CDI:USAGE_HINT:ATTACK]
 		[CDI:BP_REQUIRED:BY_CATEGORY:MOUTH]
 		[CDI:MATERIAL:LOCAL_CREATURE_MAT:MEDUSA_GAZE:TRAILING_VAPOR_FLOW]
 		[CDI:TARGET:C:LINE_OF_SIGHT]
 		[CDI:TARGET_RANGE:C:15]
 		[CDI:MAX_TARGET_NUMBER:C:1]
 		[CDI:WAIT_PERIOD:50]
	
		[ATTACK:PUNCH:BODYPART:BY_TYPE:GRASP]
			[ATTACK_SKILL:GRASP_STRIKE]
			[ATTACK_VERB:punch:punches]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_FLAG_WITH]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
		[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
			[ATTACK_SKILL:BITE]
			[ATTACK_VERB:bite:bites]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
			[ATTACK_FLAG_CANLATCH] okay
			[SPECIALATTACK_SUCK_BLOOD:25:50]
		[HOMEOTHERM:10067]
		[SWIMS_INNATE]
		[SELECT_MATERIAL:ALL]
			[MULTIPLY_VALUE:4]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:SEA_GREEN:1:FERN_GREEN:1:MOSS_GREEN:1:OLIVE:1:DARK_OLIVE:1:EMERALD:1:JADE:1]
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:GOLD:1]
				[TLCM_NOUN:eyes:PLURAL]
	[CASTE:MALE_STONEGAZER]
		[POP_RATIO:5]
		[MALE]
		[DESCRIPTION:A scaled creature with a humanoid torso and a bird head. Beware its petrifying breath!]
		[CASTE_NAME:cockatrice:cockatrices:cockatrice]
		[GENERAL_CHILD_NAME:cockatrice hatchling:cockatrice hatchlings]
		[PREFSTRING:petrifying breath]
		--no legs!
		[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:703:505:274:1900:2900] 32 kph  [SWIMS_INNATE]	
		[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:1945:1504:1062:548:3100:4500] 16 kph, slower than goat
		[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph  [STANCE_CLIMBER][NATURAL_SKILL:CLIMBING:5]

		[BODY:HUMANOID_LEGLESS_NECK:TAIL_STANCE:HUMANOID_JOINTS:5FINGERS:THROAT:NECK:SPINE:2EYES:2LUNGS:HEART:GUTS:BRAIN:SKULL:BEAK:RIBCAGE]
		[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
			[REMOVE_MATERIAL:SKIN]
			[REMOVE_MATERIAL:LEATHER]
			[REMOVE_MATERIAL:PARCHMENT]
			[REMOVE_MATERIAL:HAIR]
			[USE_MATERIAL_TEMPLATE:SCALE:THINSCALE_NOLEATHER_TEMPLATE]
		[BODY_DETAIL_PLAN:STANDARD_TISSUES]
			[REMOVE_TISSUE:SKIN]
			[REMOVE_TISSUE:HAIR]
			[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
		[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
		[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
		 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
			[TL_MAJOR_ARTERIES]
		[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
		[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
		[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
		[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
		[HAS_NERVES]
		[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
		[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
		[CREATURE_CLASS:GENERAL_POISON]
		[GETS_WOUND_INFECTIONS]
		[GETS_INFECTIONS_FROM_ROT]
		[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
		[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
		[BODY_SIZE:0:0:2000]
		[BODY_SIZE:1:168:20000]
		[BODY_SIZE:2:0:70000]
		[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
		[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
		[MAXAGE:180:200]

		[USE_MATERIAL_TEMPLATE:MEDUSA_GAZE:CREATURE_EXTRACT_TEMPLATE]
			[STATE_NAME:ALL_SOLID:frozen petrifying liquid]
			[STATE_ADJ:ALL_SOLID:frozen petrifying liquid]
			[STATE_NAME:LIQUID:petrifying liquid]
			[STATE_ADJ:LIQUID:petrifying liquid]
			[STATE_NAME:GAS:petrifying gas]
			[STATE_ADJ:GAS:petrifying gas]
			[PREFIX:NONE]
			[ENTERS_BLOOD]
			[SYNDROME]
				[SYN_NAME:petrification]
				[SYN_AFFECTED_CLASS:GENERAL_POISON]
				[SYN_IMMUNE_CREATURE:SNAKEMAN:ALL]
				[SYN_INHALED]
				[CE_PARALYSIS:SEV:800:PROB:100:RESISTABLE:SIZE_DILUTES:START:5:PEAK:10:END:200]
			[CAN_DO_INTERACTION:MATERIAL_EMISSION]
	 		[CDI:ADV_NAME:Gaze]
	 		[CDI:USAGE_HINT:ATTACK]
	 		[CDI:BP_REQUIRED:BY_CATEGORY:MOUTH]
	 		[CDI:MATERIAL:LOCAL_CREATURE_MAT:MEDUSA_GAZE:TRAILING_VAPOR_FLOW]
	 		[CDI:TARGET:C:LINE_OF_SIGHT]
	 		[CDI:TARGET_RANGE:C:15]
	 		[CDI:MAX_TARGET_NUMBER:C:1]
	 		[CDI:WAIT_PERIOD:50]
	
		[ATTACK:PUNCH:BODYPART:BY_TYPE:GRASP]
			[ATTACK_SKILL:GRASP_STRIKE]
			[ATTACK_VERB:punch:punches]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_FLAG_WITH]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
		[ATTACK:BITE:BODYPART:BY_CATEGORY:BEAK]
			[ATTACK_SKILL:BITE]
			[ATTACK_VERB:bite:bites]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_PENETRATION_PERC:100]
			[ATTACK_FLAG_EDGE]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
			--ATTACK_FLAG_CANLATCH] --do not rip limbs off with teeth!
		[HOMEOTHERM:10067]
		[SWIMS_INNATE]
		[SELECT_MATERIAL:ALL]
			[MULTIPLY_VALUE:4]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:SEA_GREEN:1:FERN_GREEN:1:MOSS_GREEN:1:OLIVE:1:DARK_OLIVE:1:EMERALD:1:JADE:1]
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:GOLD:1]
				[TLCM_NOUN:eyes:PLURAL]
	[CASTE:FEMALE_NAGA]
		[POP_RATIO:100]
		[FEMALE]
		[LITTERSIZE:3:7]
		[DESCRIPTION:A four-armed scaled creature with a body of a woman and a tail of a snake instead of legs.]
		[CASTE_NAME:white naga:white nagas:white naga]
		[GENERAL_CHILD_NAME:white naga hatchling:white naga hatchlings]
		[PREFSTRING:four arms]
		--no legs!
		[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:703:505:274:1900:2900] 32 kph  [SWIMS_INNATE]	
		[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:1945:1504:1062:548:3100:4500] 16 kph, slower than goat
		[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph  [STANCE_CLIMBER][NATURAL_SKILL:CLIMBING:5]
		
		[BODY:HUMANOID4ARMS_LEGLESS:HUMANOID4ARMS_LEGLESS_JOINTS:TAIL_STANCE:5FINGERS:SPINE:2EYES:2LUNGS:HEART:GUTS:BRAIN:SKULL:MOUTH:TEETH:RIBCAGE] --throat and neck removed.
		[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
			[REMOVE_MATERIAL:SKIN]
			[REMOVE_MATERIAL:LEATHER]
			[REMOVE_MATERIAL:PARCHMENT]
			[REMOVE_MATERIAL:HAIR]
			[USE_MATERIAL_TEMPLATE:SCALE:THINSCALE_NOLEATHER_TEMPLATE]
		[BODY_DETAIL_PLAN:STANDARD_TISSUES]
			[REMOVE_TISSUE:SKIN]
			[REMOVE_TISSUE:HAIR]
			[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
		[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
		[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
		 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
			[TL_MAJOR_ARTERIES]
		[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
		[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
		[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
		[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
		[HAS_NERVES]
		[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
		[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
		[CREATURE_CLASS:GENERAL_POISON]
		[GETS_WOUND_INFECTIONS]
		[GETS_INFECTIONS_FROM_ROT]
		[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
		[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
		[BODY_SIZE:0:0:2000]
		[BODY_SIZE:1:168:20000]
		[BODY_SIZE:2:0:70000]
		[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
		[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
		[MAXAGE:180:200]

		[USE_MATERIAL_TEMPLATE:NAGAVENOM:CREATURE_EXTRACT_TEMPLATE]
			[STATE_NAME:ALL_SOLID:frozen naga saliva]
			[STATE_ADJ:ALL_SOLID:frozen naga saliva]
			[STATE_NAME:LIQUID:naga saliva]
			[STATE_ADJ:LIQUID:naga saliva]
			[STATE_NAME:GAS:naga spray]
			[STATE_ADJ:GAS:naga spray]
			[PREFIX:NONE]
			[ENTERS_BLOOD]
			[SYNDROME]
				[SYN_NAME:naga venom]
				[SYN_AFFECTED_CLASS:GENERAL_POISON]
				[SYN_IMMUNE_CREATURE:SNAKEMAN:ALL]
				[SYN_INHALED][SYN_INJECTED]
				[CE_PARALYSIS:SEV:800:PROB:100:RESISTABLE:SIZE_DILUTES:START:3:PEAK:12:END:40]
				[CE_DIZZINESS:SEV:200:PROB:70:SIZE_DILUTES:START:3:PEAK:15:END:100]
	
		[ATTACK:PUNCH:BODYPART:BY_TYPE:GRASP]
			[ATTACK_SKILL:GRASP_STRIKE]
			[ATTACK_VERB:punch:punches]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_FLAG_WITH]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
		[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
			[ATTACK_SKILL:BITE]
			[ATTACK_VERB:bite:bites]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
			[ATTACK_FLAG_CANLATCH] okay
			[SPECIALATTACK_SUCK_BLOOD:25:50]
			[SPECIALATTACK_INJECT_EXTRACT:LOCAL_CREATURE_MAT:NAGAVENOM:LIQUID:100:100]
		[HOMEOTHERM:10067]
		[SWIMS_INNATE]
		[SELECT_MATERIAL:ALL]
			[MULTIPLY_VALUE:4]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:WHITE:1:SILVER:1:LAVENDER:1:CREAM:1]
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:IRIS_EYE_RED:1]
				[TLCM_NOUN:eyes:PLURAL]
	[CASTE:MALE_NAGA]
		[POP_RATIO:100]
		[MALE]
		[DESCRIPTION:A four-armed scaled creature with a body of a man and a tail of a snake instead of its legs.]
		[CASTE_NAME:black naga:black nagas:black naga]
		[GENERAL_CHILD_NAME:black naga hatchling:black naga hatchlings]
		[PREFSTRING:four arms]
		--no legs!
		[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:703:505:274:1900:2900] 32 kph  [SWIMS_INNATE]	
		[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:1945:1504:1062:548:3100:4500] 16 kph, slower than goat
		[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph  [STANCE_CLIMBER][NATURAL_SKILL:CLIMBING:5]

		[BODY:HUMANOID4ARMS_LEGLESS:HUMANOID4ARMS_LEGLESS_JOINTS:TAIL_STANCE:5FINGERS:SPINE:2EYES:2LUNGS:HEART:GUTS:BRAIN:SKULL:MOUTH:TEETH:RIBCAGE] --throat and neck removed.
		[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
			[REMOVE_MATERIAL:SKIN]
			[REMOVE_MATERIAL:LEATHER]
			[REMOVE_MATERIAL:PARCHMENT]
			[REMOVE_MATERIAL:HAIR]
			[USE_MATERIAL_TEMPLATE:SCALE:THINSCALE_NOLEATHER_TEMPLATE]
		[BODY_DETAIL_PLAN:STANDARD_TISSUES]
			[REMOVE_TISSUE:SKIN]
			[REMOVE_TISSUE:HAIR]
			[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
		[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
		[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
		 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
			[TL_MAJOR_ARTERIES]
		[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
		[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
		[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
		[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
		[HAS_NERVES]
		[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
		[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
		[CREATURE_CLASS:GENERAL_POISON]
		[GETS_WOUND_INFECTIONS]
		[GETS_INFECTIONS_FROM_ROT]
		[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
		[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
		[BODY_SIZE:0:0:2000]
		[BODY_SIZE:1:168:20000]
		[BODY_SIZE:2:0:70000]
		[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
		[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
		[MAXAGE:180:200]

		[USE_MATERIAL_TEMPLATE:NAGAVENOM:CREATURE_EXTRACT_TEMPLATE]
			[STATE_NAME:ALL_SOLID:frozen naga saliva]
			[STATE_ADJ:ALL_SOLID:frozen naga saliva]
			[STATE_NAME:LIQUID:naga saliva]
			[STATE_ADJ:LIQUID:naga saliva]
			[STATE_NAME:GAS:boiling naga saliva]
			[STATE_ADJ:GAS:boiling naga saliva]
			[PREFIX:NONE]
			[ENTERS_BLOOD]
			[SYNDROME]
				[SYN_NAME:naga venom]
				[SYN_AFFECTED_CLASS:GENERAL_POISON]
				[SYN_IMMUNE_CREATURE:SNAKEMAN:ALL]
				[SYN_INHALED]
				[CE_PARALYSIS:SEV:800:PROB:100:RESISTABLE:SIZE_DILUTES:START:5:PEAK:10:END:20]
	
		[ATTACK:PUNCH:BODYPART:BY_TYPE:GRASP]
			[ATTACK_SKILL:GRASP_STRIKE]
			[ATTACK_VERB:punch:punches]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_FLAG_WITH]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
		[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
			[ATTACK_SKILL:BITE]
			[ATTACK_VERB:bite:bites]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_PRIORITY:MAIN]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_FLAG_CANLATCH] okay
			[SPECIALATTACK_SUCK_BLOOD:25:50]
			[SPECIALATTACK_INJECT_EXTRACT:LOCAL_CREATURE_MAT:NAGAVENOM:LIQUID:100:100]
		[HOMEOTHERM:10067]
		[SWIMS_INNATE]
		[SELECT_MATERIAL:ALL]
			[MULTIPLY_VALUE:4]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:BLACK:1:SLATE_GRAY:1]
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:IRIS_EYE_RED:1]
				[TLCM_NOUN:eyes:PLURAL]
	[CASTE:FEMALE_LIZARDFOLK]
		[POP_RATIO:170]
		[FEMALE]
		[LITTERSIZE:3:7]
		[DESCRIPTION:A humanoid lizard resembling a bipedal crocodile. The air around her sparkles with dark magic.]
		[CASTE_NAME:lizardfolk witch:lizardfolk witches:lizardfolk witch]
		[GENERAL_CHILD_NAME:lizardfolk hatchling:lizardfolk hatchlings]
		[PREFSTRING:barbed tails]
	
		[APPLY_CREATURE_VARIATION:STANDARD_BIPED_GAITS:900:675:450:225:1900:2900] 39 kph BIPED		
		[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:703:505:274:1900:2900] 32 kph  [SWIMS_INNATE]	
		[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph
		[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:96561:6115:5683:1755:7456:8567] 5 kph  [STANCE_CLIMBER][NATURAL_SKILL:CLIMBING:5]

		[BODY:HUMANOID_NECK:HUMANOID_JOINTS:TAIL:5FINGERS:5TOES:THROAT:NECK:SPINE:2EYES:2LUNGS:HEART:GUTS:BRAIN:SKULL:MOUTH:TEETH:RIBCAGE]
		[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
			[REMOVE_MATERIAL:SKIN]
			[REMOVE_MATERIAL:LEATHER]
			[REMOVE_MATERIAL:PARCHMENT]
			[REMOVE_MATERIAL:HAIR]
			[USE_MATERIAL_TEMPLATE:SCALE:THINSCALE_NOLEATHER_TEMPLATE]
		[BODY_DETAIL_PLAN:STANDARD_TISSUES]
			[REMOVE_TISSUE:SKIN]
			[REMOVE_TISSUE:HAIR]
			[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
		[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
		[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
		 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
			[TL_MAJOR_ARTERIES]
		[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
		[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
		[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
		[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
		[HAS_NERVES]
		[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
		[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
		[CREATURE_CLASS:GENERAL_POISON]
		[GETS_WOUND_INFECTIONS]
		[GETS_INFECTIONS_FROM_ROT]
		[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
		[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
		[BODY_SIZE:0:0:2000]
		[BODY_SIZE:1:168:20000]
		[BODY_SIZE:2:0:70000]
		[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
		[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
		[MAXAGE:180:200]
		[ATTACK:PUNCH:BODYPART:BY_TYPE:GRASP]
			[ATTACK_SKILL:GRASP_STRIKE]
			[ATTACK_VERB:punch:punches]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_FLAG_WITH]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
		[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
			[ATTACK_SKILL:BITE]
			[ATTACK_VERB:bite:bites]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
			[ATTACK_FLAG_CANLATCH] okay
			[SPECIALATTACK_SUCK_BLOOD:25:50]
		[HOMEOTHERM:10067]
		[SWIMS_INNATE]
		[SELECT_MATERIAL:ALL]
			[MULTIPLY_VALUE:4]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:FERN_GREEN:1:EMERALD:1:DARK_GREEN:1:DARK_OLIVE:1]
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:IRIS_EYE_BLACK:1]
				[TLCM_NOUN:eyes:PLURAL]
	[CASTE:MALE_LIZARDFOLK]
		[POP_RATIO:170]
		[MALE]
		[DESCRIPTION:A humanoid lizard resembling a bipedal crocodile. His eyes seem to glow, and his face and chest have painted symbols.]
		[CASTE_NAME:lizardfolk mystic:lizardfolk mystics:lizardfolk mystic]
		[GENERAL_CHILD_NAME:lizardfolk hatchling:lizardfolk hatchlings]
		[PREFSTRING:barbed tails]
		[APPLY_CREATURE_VARIATION:STANDARD_BIPED_GAITS:900:675:450:225:1900:2900] 39 kph BIPED
		[APPLY_CREATURE_VARIATION:STANDARD_SWIMMING_GAITS:900:703:505:274:1900:2900] 32 kph  [SWIMS_INNATE]	
		[APPLY_CREATURE_VARIATION:STANDARD_CRAWLING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph
		[APPLY_CREATURE_VARIATION:STANDARD_CLIMBING_GAITS:6561:6115:5683:1755:7456:8567] 5 kph  [STANCE_CLIMBER][NATURAL_SKILL:CLIMBING:5]

		[BODY:HUMANOID_NECK:HUMANOID_JOINTS:TAIL:5FINGERS:5TOES:THROAT:NECK:SPINE:2EYES:2LUNGS:HEART:GUTS:BRAIN:SKULL:MOUTH:TEETH:RIBCAGE]
		[BODY_DETAIL_PLAN:STANDARD_MATERIALS]
			[REMOVE_MATERIAL:SKIN]
			[REMOVE_MATERIAL:LEATHER]
			[REMOVE_MATERIAL:PARCHMENT]
			[REMOVE_MATERIAL:HAIR]
			[USE_MATERIAL_TEMPLATE:SCALE:THINSCALE_NOLEATHER_TEMPLATE]
		[BODY_DETAIL_PLAN:STANDARD_TISSUES]
			[REMOVE_TISSUE:SKIN]
			[REMOVE_TISSUE:HAIR]
			[USE_TISSUE_TEMPLATE:SCALE:SCALE_TEMPLATE]
		[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SCALE:FAT:MUSCLE:BONE:CARTILAGE]
		[SELECT_TISSUE_LAYER:HEART:BY_CATEGORY:HEART]
		 [PLUS_TISSUE_LAYER:SKIN:BY_CATEGORY:THROAT]
			[TL_MAJOR_ARTERIES]
		[BODY_DETAIL_PLAN:STANDARD_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_HEAD_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RIBCAGE_POSITIONS]
		[BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
		[USE_MATERIAL_TEMPLATE:SINEW:SINEW_TEMPLATE]
		[TENDONS:LOCAL_CREATURE_MAT:SINEW:200]
		[LIGAMENTS:LOCAL_CREATURE_MAT:SINEW:200]
		[HAS_NERVES]
		[USE_MATERIAL_TEMPLATE:BLOOD:BLOOD_TEMPLATE]
		[BLOOD:LOCAL_CREATURE_MAT:BLOOD:LIQUID]
		[CREATURE_CLASS:GENERAL_POISON]
		[GETS_WOUND_INFECTIONS]
		[GETS_INFECTIONS_FROM_ROT]
		[USE_MATERIAL_TEMPLATE:PUS:PUS_TEMPLATE]
		[PUS:LOCAL_CREATURE_MAT:PUS:LIQUID]
		[PHYS_ATT_RANGE:STRENGTH:1250:1500:1750:2000:2500:3000:5000]
		[PHYS_ATT_RANGE:TOUGHNESS:1250:1500:1750:2000:2500:3000:5000]
		[PHYS_ATT_RANGE:AGILITY:700:1200:1400:1500:1600:1800:2500]
		[BODY_SIZE:0:0:2000]
		[BODY_SIZE:1:168:20000]
		[BODY_SIZE:2:0:70000]
		[BODY_APPEARANCE_MODIFIER:HEIGHT:90:95:98:100:102:105:110]
		[BODY_APPEARANCE_MODIFIER:BROADNESS:90:95:98:100:102:105:110]
		[MAXAGE:180:200]
		[ATTACK:PUNCH:BODYPART:BY_TYPE:GRASP]
			[ATTACK_SKILL:GRASP_STRIKE]
			[ATTACK_VERB:punch:punches]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_FLAG_WITH]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
		[ATTACK:BITE:BODYPART:BY_CATEGORY:MOUTH]
			[ATTACK_SKILL:BITE]
			[ATTACK_VERB:bite:bites]
			[ATTACK_CONTACT_PERC:100]
			[ATTACK_PREPARE_AND_RECOVER:3:3]
			[ATTACK_PRIORITY:MAIN]
			[ATTACK_FLAG_CANLATCH] okay
			[SPECIALATTACK_SUCK_BLOOD:25:50]
		[HOMEOTHERM:10067]
		[SWIMS_INNATE]
		[SELECT_MATERIAL:ALL]
			[MULTIPLY_VALUE:4]
		[SET_TL_GROUP:BY_CATEGORY:ALL:SCALE]
			[TL_COLOR_MODIFIER:FERN_GREEN:1:EMERALD:1:DARK_GREEN:1:DARK_OLIVE:1:AZURE:1:PINE_GREEN:1:TEAL:1:CERULEAN:1]
				[TLCM_NOUN:scales:PLURAL]
		[SET_TL_GROUP:BY_CATEGORY:EYE:EYE]
			[TL_COLOR_MODIFIER:IRIS_EYE_RED:1]
				[TLCM_NOUN:eyes:PLURAL]
	[SELECT_CASTE:ALL]
		[SELECT_MATERIAL:BLOOD]
			[STATE_COLOR:ALL_SOLID:GREEN]
			[STATE_COLOR:LIQUID:GREEN]
			[STATE_COLOR:GAS:GREEN]
