entity_monster

[OBJECT:ENTITY]

=====================================================================================================================================
Old versions' adventure tiers:
	1 - dwarves
	2 - sylvan elves
	3 - dusk elves
	4 - dunedwellers
	5 - keepers
	6 - nords
	7 - orcs
	8 - goblins
	9 - serpentfolk
	10 - kobolds
	11 - dark dwarves
	12 - illithids
	13 - beastmen
	14 - centaurs
	15 - scavengers
	16 - demons
	
	17 - hydra
	18 - red dragon
	19 - blue dragon
	20 - wyvern
	21 - balor
	22 - deep crow
	23 - cave dragon
	24 - roc
	25 - bronze colossus
	26 - giant
	27 - cyclops
	28 - ettin
	29 - minotaur
	30 - thunderbird

=====================================================================================================================================

[ENTITY:ENTITY_HYDRA]
	[INDIV_CONTROLLABLE]
	[CREATURE:HYDRA]
	[TRANSLATION:LATIN]

[ENTITY:ENTITY_DRAGON_RED]
	[INDIV_CONTROLLABLE]
	[CREATURE:DRAGON_RED]
	[TRANSLATION:LATIN]

[ENTITY:ENTITY_DRAGON_BLUE]
	[INDIV_CONTROLLABLE]
	[CREATURE:DRAGON_BLUE]
	[TRANSLATION:LATIN]

[ENTITY:ENTITY_WYVERN]
	[INDIV_CONTROLLABLE]
	[CREATURE:DRAGON_GREEN]
	[TRANSLATION:LATIN]

[ENTITY:ENTITY_BALOR]
	[INDIV_CONTROLLABLE]
	[CREATURE:BALOR_DETAILED]
	[TRANSLATION:LATIN]

[ENTITY:ENTITY_CROW_DEEP]
	[INDIV_CONTROLLABLE]
	[CREATURE:BIRD_CROW_DEEP]
	[TRANSLATION:LATIN]

[ENTITY:ENTITY_DRAGON_CAVE]
	[INDIV_CONTROLLABLE]
	[CREATURE:CAVE_DRAGON]
	[TRANSLATION:LATIN]

[ENTITY:ENTITY_ROC]
	[INDIV_CONTROLLABLE]
	[CREATURE:BIRD_ROC]
	[TRANSLATION:LATIN]

[ENTITY:ENTITY_COLOSSUS_BRONZE]
	[INDIV_CONTROLLABLE]
	[CREATURE:COLOSSUS_BRONZE]
	[TRANSLATION:LATIN]
	
[ENTITY:ENTITY_GIANT]
	[INDIV_CONTROLLABLE]
	[CREATURE:GIANT]
	[TRANSLATION:LATIN]
	
[ENTITY:ENTITY_CYCLOPS]
	[INDIV_CONTROLLABLE]
	[CREATURE:CYCLOPS]
	[TRANSLATION:LATIN]
	
[ENTITY:ENTITY_ETTIN]
	[INDIV_CONTROLLABLE]
	[CREATURE:ETTIN]
	[TRANSLATION:LATIN]
	
[ENTITY:ENTITY_MINOTAUR]
	[INDIV_CONTROLLABLE]
	[CREATURE:MINOTAUR]
	[TRANSLATION:LATIN]
	
[ENTITY:ENTITY_THUNDERBIRD]
	[INDIV_CONTROLLABLE]
	[CREATURE:BIRD_THUNDERBIRD]
	[TRANSLATION:LATIN]